\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

%\usepackage{hyperref}

%\hypersetup{
%    colorlinks,
%    linkcolor={red!50!black},
%    citecolor={blue!50!black},
%    urlcolor={blue!99!black}
%}

\usepackage{titling}

\pretitle{%
\includegraphics[width=0.3\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Instabilities and turbulence course final report 2022-2023}
\author{Ruben ESPELETA BOLIVAR}

% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}

% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\maketitle

\tableofcontents

\newpage

\section{Important concepts in instabilities and turbulence}

\subsection{Introduction}
Turbulence is defined as the state of a fluid when its flow is irregular such that velocity varies randomly in every single point of the space where it is defined \cite[]{cadot2013}. Everyone can observe in our daily life that the fluid motions are indeed complex, disorganized and do not follow specifics patrons. In fact, it is difficult to find a fluid whose motion is regular and one may think that real fluid motions are irregular and unstable because the forces that provide them with that motion are irregular and unstable, or because the external conditions are also like that. But, in reality fluid motions are turbulent and unstable by nature. 
In a well controlled experiment in a laboratory, we can observed that the flow around an obstacle undergoing a stationary force will irremediably transit towards a disordered time-dependent flow, above a certain value of velocity U.

This unsteady response to a stationary excitation shows the non-linear character of the fluid dynamics. We have the fluid dynamic equation, we know the non-linear term but the non-uniqueness of the solutions does not give any hope of trying to find analytical solutions in real cases. Moreover, from a mathematical point of view, this equation has still not delivered all its mysteries \cite[]{cadot2013}.

Currently, turbulence is at heart of the fundamental problems in classical physics. It is characterized by the existence of motions of all sizes, unlike laminar flow which takes place at a precise scale. These movements correspond, among other things,to vortices of different size, the smallest of which are transported by the largest.

\subsection{Navier-Stokes equation}
The Navier-Stokes equation can be demonstrated from a balance of momentum per unit volume for an incompressible fluid. 
\begin{equation}
	\vec{\nabla}.\vec{v}=0,
\end{equation}
\begin{equation}
	\rho\frac{\partial\vec{v}}{\partial t} + \rho (\vec{v}. \vec{\nabla})\vec{v}=-\vec{\nabla}p + \rho\vec{f_e} + \mu\Delta\vec{v},
\end{equation}
The first equation of divergence nule of the velocity field $\vec{v}(\vec{r},t)$ ensures the incompressibility of the fluid. Each term of the second equation is a force per unit volume. $\rho$ denotes the density of the fluid and $p(\vec{r},t)$ the pressure, which by definition will be the isotropic part of the stress tensor. The $-\vec{\nabla}p$ term thus represents normal stresses related to pressure forces \cite[]{cadot2013}.

The force per unit mass $\vec{f_e}(\vec{r},t)$, includes all the forces applied in the volume of the fluid. Often, only gravitational forces are present. In this case, we call $p_0$ the value that the pressure takes in the absence of flow, $\vec{v}=0$ and $\vec{\nabla} p_0=\rho \vec{g}$ (hydrostatic pressure). For these flows, the fluid is set in motion by a velocity-related forcing imposed by the boundary conditions. Finally, $\mu$ is the dynamic viscosity of the fluid. 

Dividing by the density $\rho$, the force balance equation (2) of forces per unit volume, we obtain the force balance per unit mass, which can also be interpreted as a transport equation of the velocity:

\begin{equation}
	\frac{\partial \vec{v}}{\partial t} + (\vec{v}.\vec{\nabla})\vec{v} = -\frac{1}{\rho}\vec{\nabla}(p-p_0) + \nu\Delta\vec{v}
\end{equation}

This form of the equation shows the kinematic victosity $\nu= \frac{\mu}{\rho}$. The last term of the right, $\nu\Delta\vec{v}$ which is a linear term, represents the transport of momentum (per unit mass) by molecular diffusion.

The second term of the left side part, $(\vec{v}.\vec{\nabla})\vec{v}$ is obtained from the lagrangian derivative of the velocity. This corresponds to the convective transport of the velocity, and contrary to diffusive transport, it is not linear because it is a quadratic form of the velocity.

\subsection{Diffusive transport of momentum}
Molecular diffusion is basically a purely random process of independent events \cite{cadot2013}. Each molecule of fluid interacts with the others in such a way that they are vectors of exchange physical quantities(energy or heat, mass). It is the thermal agitation of all molecules that ensures the incoherence at the molecular scale. This result is a statistically very coherent macroscopic transport of this quantity through the system. The most telling example is the transport of temperature through a metal bar initially heated on one side. The structure of the equation of transport by diffusion is written for any quantity X such that:
\begin{equation}
	\frac{\partial X}{\partial t}=D\Delta X,
\end{equation}
Where $D$ is the diffusion coefficient per quantity of X transported. The diffusion coefficient only depends on the fluid. Dimensionally, we have $[D]=L²T^-1$. From this equation we derive a characteristic time corresponding to the time needed to diffuse over a distance $\delta$:
\begin{equation}
	\tau_D=\frac{\delta^2}{D},
\end{equation}

Now, taking the dynamics equation (3) and setting the nonlinear term equal to 0, and considering the case of an intially parallel flow $\vec{v}=u(y)\vec{e}_x$ without pressure gradient along $\vec{e}_x$, the equation of the dynamics is then written:
\begin{equation}
	\frac{\partial \vec{v}}{\partial t}= \nu\Delta\vec{v};
\end{equation}
This is a diffusion equation just like (4). The vector quantity that is transported by molecular diffusion is the momentum per unit mass. The coefficient of diffusion $D$ is the kinematic viscosity $\nu$. Since the equation is linear, the flow will remain paralell throughout its dynamics. 

\subsection{Convective transport}
In the case of convective transport, it is the velocity itself that carries the amount of momentum \cite[]{cadot2013}. This time, we consider the equation of the dynamics without the diffusive term. In this case, the equation is non-linear (euler equation):
\begin{equation}
	\frac{\partial\vec{v}}{\partial t} + (\vec{v}.\vec{\nabla})\vec{v}= -\frac{1}{\rho}\vec{\nabla}(p-p_0)
\end{equation}
In the case of non-potential flows (which is the case of turbulance), we face the problem of the existence and uniqueness solution. Often, analytical solution does not exist and we have to resort to numerical simulations. From the same initial conditions as for a diffusive transport, we observe a very different evolution: the flow does not remain paralell, and a vortex is formed \cite[]{cadot2013}. This vortex of peripheral velocity $U$ will transport momentum between an upper and lower part.The vortex grows over time as $\delta  \alpha \Delta Ut$. With this mechanism, the characteristic time to transport velocity over a lenght $\delta$, this is, the advective time, is:
\begin{equation}
	\tau_a=\frac{\delta}{U}.
\end{equation}
\subsection{Reynolds number}
We can write the Navier-Stokes equation using dimensionless combinations of the different quantities involved \cite[]{cadot2013}. Let $L$ and $U$ be the respective scales of size and velocity of the flow, respectively. We have:
\begin{equation}
	\vec{r}=L\vec{r}' ; \vec{v}=U\vec{v}' ; p-p_0=(\rho U²)p' ; t= \frac{L}{U} t',
\end{equation}

And the Navier-stokes equation becomes:
\begin{equation}
	\frac{\partial\vec{v}'}{\partial t'} + (\vec{v}'.\vec{\nabla}')\vec{v}'=-\vec{\nabla}'p'+\frac{1}{Re}\Delta'\vec{v}'
\end{equation}
\begin{equation}
	Re=\frac{UL}{\nu};
\end{equation}
Here appears a dimensionless number, $Re$ which is a combination of $L$, $U$ and $\nu$: The reynolds number.It measures the viscous diffusion term with respect to the other terms of the equation. This number can be read as the ratio of two times: the characteristic time required to transport the momentum through a distance L by diffusion and convection. 
\begin{equation}
	Re=\frac{UL}{\nu}=\frac{\frac{L²}{\nu}}{\frac{L}{U}}=\frac{\tau_\nu}{\tau_c}
\end{equation}
This is one of the most important parameters in fluid dynamics. The nature of the solutions of the equation will depend crucially on the value of the Reynolds number. If $Re$<<1, the equation is linear since the diffusive phenomenon dominates, if $Re$>>1, the equation is non-linear because the convective phenomenon dominates. The non linear effects for a stationary forcing, breaks in symmetries with respect to the initial boundary conditions, so, it will create turbulance \cite[]{cadot2013}.

\section{Instabilities of systems with few degrees of freedom}
\subsection{Lotka-Volterra equations}
With the purpose of analizing the stability of fixed points, in this course we proposed a system of two dimension differential equations, also known as the Lotka-Volterra equations, as follows:
\begin{equation}
	\frac{dX}{dt}=AX-BXY,
\end{equation}
\begin{equation}
	\frac{dY}{dt}=-CY+DXY,
\end{equation}
Where X(t) and Y(t) represent the amount of individuals for the prey and the predator, as a function of time, respectively. The parameters A, B, C and D are real positive constants. For the equation that describes the rate of change of the preys, it is a function of the number of preys, and also a function of the number of predators, since the greatest the number of predators, the number of preys will decrease; multiplied by a factor of death B function of the number of preys. 
For the function that represents the rate of change of predators, it is a function of the number of predators multiplied by a factor D, and also by the number of preys, since the greater this number, the predators will increase their population. Also, it is function of a factor of death that is a function of the number of predators.
For this model, we proceed to model it using iphyton, setting initial arbitary values different than 0 for the initial conditions and we set the time pass from 0,1 to 20 and we obtain the result showed in figure~\ref{fig_simple_2}.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.9\linewidth]{../fig/fig_simple_2}
	\caption{Plot result of the evolution of species for predator and prey as a function of time.}
	\label{fig_simple_2}
\end{figure}
According to the simulation, we can see how initally after setting up a number of prey larger that predators, this number starts decreasing as the number of predators starts increasing. However, after certain time, the number of predators reaches a maximum and then starts decreasing, so for this case the number of preys continues decreasing but since at a certain point the number of predators has decreased so much, eventually they will increase again, and we get this kind of oscillations between the two species.

So, to analize this system, we also can simulate a phase plane which is just a plane of X and Y that allows us to see the ways pathways that this variables takes as predicted by the dynamics equations. What we do is that we have to find what we call nullclines, which are lines in which $\frac{dX}{dt}$ or $\frac{dY}{dt}$ is 0. So, meaning that the derivative are equal to cero implies that:
\begin{equation}
	X(A-BY)=0;
\end{equation}
\begin{equation}
	-Y(C-DX)=0;
\end{equation}
For (15) whether $X=0$ or $Y=\frac{A}{B}$, and for (16) whether $Y=0$ or $X=\frac{C}{D}$. So, for the first nullclines we need to see that since $\frac{dX}{dt}=0$, only $Y$ is changing when we are around these lines, and for $\frac{dY}{dt}=0$, only X is changing. For the first values that we enter in the simulation for $X=2$ and $Y=1$ as initial conditions, we  found that this would be our system we have these two fixed points, the first one at the origin, and the second one at $X=2$ and $Y=1$, for our simulation as shown in figure \ref{fig_simple}.

So, in order to know how the trajectory behaves, if the fixed points are stable and they atract trajectories towards to them, or if they are unstable and the trajectories move away, we make the simulation and we get the figure \ref{fig_simple}, which shows us that the first point is actually unstable, and the trajectory move away from it,which makes sense because the eigen values of the jacobian matrix for this fixed point are real values.

For the second fixed point, we see that is actually the center of the graph, and that it atracts the trajectory towards this point. This point is actually stable, and the eigen values of the jacobian matrix for this fixed point are imaginary. 

We can also show that there is a conserved quantity, for which exists a combination of variable that makes the time derivative equal to 0, such that:
\begin{equation}
	U(X,Y)=Clog(X)-DX+Alog(Y)-BY=Constant;
\end{equation}
and this proves that these quantities are conserved along any given trajectory, being closed loops. It means that the state of the system remains predictable and periodic for all times, without sensitivity to initial conditions. We can simulate this for different values of initial conditions and we obtain figure \ref{fig_isocountour_pot_preypreda}.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/fig_simple}
	\caption{Trajectories of the values of X and Y towards the stable fixed point for the predator-prey simulation.}
	\label{fig_simple}
\end{figure}
\begin{figure}[!h]
		\centering
	\includegraphics[width=0.7\linewidth]{../fig/fig_isocountour_pot_preypreda}
	\caption{Isolines of potential for the prey-predator model.}.
	\label{fig_isocountour_pot_preypreda}
\end{figure}
\subsection{The Lorenz-Model (chaos)}
In the previous model we studied, we could see how the system of 2 degrees of freedom can be predictable and we modeled the equations for different initial values and saw the behavior of the functions using matpolib. However, for this particular case we can see the Lorenz model which represents the chaos for a system. The Lorenz model is inspired by the convection of the atmosphere, given by the differential equations:
\begin{equation}
	\frac{dX}{dt}=\sigma(Y-X);
\end{equation}
\begin{equation}
	\frac{dY}{dt}=\rho X-Y-XZ;
\end{equation}
\begin{equation}
	\frac{dZ}{dt}=XY-\beta Z;
\end{equation}
In this case, the values of the initial conditions $\sigma$, $\rho$ and $\beta$ are very important and they determine the behavior of the system. 
For this case, we proceed the same way as we did before, and setting initial conditions we run the simulation to observe the behavior of the trajectories around the fixed points.

We can see in Figure \ref{fig_job_sim_lorenz_2} only a 2D representation of the behavior of the trajectory of the variables X and Y around the nullclines, and we can see how, if we change the parameters of the initial conditions in the simulation, we obtain a different behavior of the variable around the fixed points in figure \ref{fig_job_sim_lorenz_3} (in this particular case for our second simulation, we note that the trajectories of X and Y get farther from the nullclines).
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/fig_job_sim_lorenz_2}
	\caption{Trajectories of the values of X and Y towards the stable fixed points for chaos system.}
	\label{fig_job_sim_lorenz_2}
\end{figure}
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/fig_job_sim_lorenz_3}
	\caption{Trajectories of the values of X and Y towards the stable fixed point after changes in initial conditions for Lorenz-Model.}
	\label{fig_job_sim_lorenz_3}
\end{figure}
\section{Bernard-Marangoni thermocapillary instability}
The so-called Benard-Marangoni instability appears when the lower surface of a thin layer of liquid, whose upper surface is free: it is due to the forces induces on the free surface by surface tension gradients \cite[]{guyon2001hydrodynamique}. This phenomenon has been studied by Benard at the beginning of this century, and has long been confused with the Rayleigh-Benard instability (this one is due to the forces in volume of Archimedean thrust and is only active for a volume limited bounded by two solid walls). Above a critical ATC value of the temperature difference, hexagonal flow cells appear between the base of the liquid layer and the free surface. The movement of the fluid is upward in the center of the hexagons and downward at the edges as shown in Figure \ref{Instability_benard_marangoni}. The size of the hexagons is of the same order of magnitude as the thickness of the fluid layer.
The qualitative mechanism of the instability is as follows: suppose that at a point on the free surface the temperature rises slightly by an amount of $\theta$.
As a result of the decrease of the surface tension when the temperature increases, the fluid is expelled radially from this hotter region to the outside. In order to maintain the flow rate, there is an upwelling of hotter fluid coming from the lower part of the cell : this heat contribution reinforces the initial disturbance. Thus, the driving force behind the instability is the difference in surface tension between the parts of the liquid at different temperatures. As for the Rayleigh-Benard instability,
the stabilizing processes are thermal diffusion and viscosity which,
respectively, uniform the temperature distribution and slow down the motion of the fluid particles. 

The dimensionless parameter that governs this instability is the Marangoni number $Ma$ which verifies:
\begin{equation}
	Ma=\frac{b\gamma\Delta Ta}{\eta k}
\end{equation}
Where $b=-(\frac{1}{\gamma})(\frac{\partial\gamma}{\partial T})$ represents the relative rate of change of the surface tension with the temperature T defined in section 1.4.1 of chapter 1 by \cite{guyon2001hydrodynamique}, and $a$ is the thickness of the fluid layer. \cite{guyon2001hydrodynamique} also showed that, as for the Rayleigh-Benard and Taylor-Couette instabilities, $Ma$ is the ratio of a driving force (due to surface tension variations) and a viscous braking force. This ratio is therefore equal to the Marangoni number given by equation (21): it is its value that determines the possible existence of instability.
The ratio of the Rayleigh number to the Marangoni number verifies :
\begin{equation}
	\frac{Ra}{Ma}=\frac{\alpha\rho g a²}{\mid\frac{d\gamma}{dT}\mid}=\frac{\mid\delta\rho\mid ga²}{\mid\delta\gamma\mid}
\end{equation}
Where $\delta\rho$ and $\delta\gamma$ are the absolute values of the respective variations of density and of surface tension induced by the same temperature difference $\delta T$.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\linewidth]{../fig/Instability_benard_marangoni}
	\caption{Scheme of the flow in the cells of the Benard-Marangoni instability.}
	\label{Instability_benard_marangoni}
\end{figure}
\bibliographystyle{jfm}
\bibliography{./biblio}
\end{document}
